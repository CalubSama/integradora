﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        private Conexion conexion;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            lblTitle.Text = "Titulo del Punto de venta";
            lblTitle.Location = new Point(this.Width / 2 - lblTitle.Width / 2, 0);
            lblHrFecha.Location = new Point(this.Width / 2 - lblHrFecha.Width / 2, lblTitle.Height);
            lblNombre.Text = "Jehú Caleb Terán";
            lblNombre.Location = new Point(this.Width / 2 - lblNombre.Width / 2, lblHrFecha.Height + lblTitle.Height);
            DGEstilo.Location = new Point(10, lblNombre.Height + lblHrFecha.Height + lblTitle.Height);
            DGEstilo.Width = this.Width - 20;
            DGEstilo.Height = (this.Height / 3) * 2; //int.Parse((this.Height * 0.67f).ToString());
            txtEstilo.Width = this.Width - 10;
            txtEstilo.Location = new Point(10, this.Height - txtEstilo.Height - 5);
            txtEstilo.Location = new Point(10, this.Height - txtEstilo.Height - 5);
            txtEstilo.Location = new Point(10, this.Height - txtEstilo.Height - 5);
            txtEstilo.Location = new Point(10, this.Height - txtEstilo.Height - 5);
            txtEstilo.Location = new Point(10, this.Height - txtEstilo.Height - 5);
            txtEstilo.Location = new Point(10, this.Height - txtEstilo.Height - 5);
            int borde = lblHrFecha.Height + lblNombre.Height + lblTitle.Height + DGEstilo.Height;
            int espacio = (this.Height -borde - txtEstilo.Height)/2 ;
            txtEstilo.Text = espacio.ToString();
            btnEstilo.Height = this.Height - borde - txtEstilo.Height -6;
            btnEstilo.Location = new Point(10, borde + espacio - btnEstilo.Height/2);

            btnEstilo1.Location = new Point(10+ btnEstilo.Width, borde + espacio - btnEstilo.Height / 2);
            btnEstilo1.Height = this.Height - borde - txtEstilo.Height - 6;
            btnEstilo2.Location = new Point(10 + btnEstilo.Width + btnEstilo1.Width, borde + espacio - btnEstilo.Height / 2);
            btnEstilo2.Height = this.Height - borde - txtEstilo.Height - 6;
            btnEstilo3.Location = new Point(10 + btnEstilo.Width + btnEstilo1.Width + btnEstilo2.Width, borde + espacio - btnEstilo.Height / 2);
            btnEstilo3.Height = this.Height - borde - txtEstilo.Height - 6;
            btnEstilo4.Location = new Point(10 + btnEstilo.Width + btnEstilo1.Width + btnEstilo2.Width + btnEstilo3.Width, borde + espacio - btnEstilo.Height / 2);
            btnEstilo4.Height = this.Height - borde - txtEstilo.Height - 6;
            DGEstilo.Columns[0].Width = DGEstilo.Width / 6;
            DGEstilo.Columns[1].Width = DGEstilo.Width / 2;
            DGEstilo.Columns[2].Width = DGEstilo.Width / 6;
            DGEstilo.Columns[3].Width = DGEstilo.Width / 6;
            lblTotal.Text = "Total: $ 0.00";
            lblTotal.Location = new Point(DGEstilo.Width - DGEstilo.Columns[3].Width, borde + espacio);
            conexion = new Conexion();
            pbEstiloIzq.Location = new Point(0, 0);
            pbEstiloIzq.Height = lblNombre.Height + lblHrFecha.Height + lblTitle.Height - 1;
            pbEstiloDer.Location = new Point(this.Width-pbEstiloDer.Width, 0);
            pbEstiloDer.Height = lblNombre.Height + lblHrFecha.Height + lblTitle.Height - 1;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            lblHrFecha.Text = DateTime.Now.ToLongDateString() + " " + DateTime.Now.ToLongTimeString();
        }

        private void txtEstilo_KeyPress(object sender, KeyPressEventArgs e)
        {
           string  query = "SELECT * from productos WHERE codigo =" + txtEstilo.Text + ";";
            if (e.KeyChar == 13)
            {
                if (Conexion.debug)
                {
                    MessageBox.Show("Buscando productos" + query, "Mensaje 1", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                string[] productos = conexion.bucarProducto(query);
                if (productos[0] != "-1")
                { 
                    DGEstilo.Rows.Add(productos[2],productos[1],"1",productos[2], productos[2]);
                    total();
                    txtEstilo.Clear();
                    txtEstilo.Focus();
                }
            }
            else if (e.KeyChar== 27)
            {
                if (Conexion.debug)
                    MessageBox.Show("Presionaste esc", "Mensaje 2");

                cancelarUltimoProducto();
                total();
            }
        }
        private void total()
        {
            double total = 0.0;
            for (int i = 0; i < DGEstilo.Rows.Count; i++)
            {
                total += double.Parse(DGEstilo[3, i].Value.ToString());
            }
            //Total: $ 0.00
            lblTotal.Text = "Total: $ "  + total.ToString();
        }

        private void Form1_Resize(object sender, EventArgs e)
        {
            Form1_Load(sender,e);
        }
        private void cancelarUltimoProducto()
        {
            if (DGEstilo.Rows.Count >1)
            {
                DGEstilo.Rows.RemoveAt(DGEstilo.Rows.Count - 1);
            }
        }
    }
}
