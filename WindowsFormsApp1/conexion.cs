﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    class Conexion
    {
        string usuario = "root";
        string bd = "punto_de_venta";
        string pwd = "jctd";
        string host = "127.0.0.1";

        string error;
        public static bool debug = false;
        MySqlConnection con;
        MySqlCommand com;
        MySqlDataReader dr;

        private bool conectar()
        {

            string conexion = "Server =" + host + "; Database = " + bd + "; Uid =" + usuario + "; Pwd =" + pwd +  ";";
            con = new MySqlConnection(conexion);
            try
            {
                con.Open();
                return true;

            }
            catch (Exception ex)
            {
                error = ex.ToString();
                if (debug)
                {
                    MessageBox.Show(error,"Mensaje 3");
                }

            }
            return false;
        }
        public string[] bucarProducto(string comtext)
        {
            string[] productos = new string[3];
            conectar();
            com = new MySqlCommand(comtext, con);
            try
            {
                dr = com.ExecuteReader();
                if (dr.HasRows)
                {
                    if (debug)
                    {
                        MessageBox.Show("Encontre algoooo omg", "Mensaje 4");
                    }
                   
                    dr.Read();

                    productos[0] = dr.GetInt32(0).ToString();
                    productos[1] = dr.GetString(1);
                    productos[2] = dr.GetFloat(2).ToString();
                    return productos;
                }
                else
                {
                    if (debug)
                    {
                        MessageBox.Show("aiudaaaaa", "Mensaje 5");
                    }
                    productos[0] = "-1";
                }
            
            }
            catch (Exception ex)
            {
                error = ex.ToString();
                if (debug)
                {
                    MessageBox.Show(error, "Mensaje 6");
                }
                productos[0] = "-1";
                
            }

            return productos;
        }
        private bool desconectar()
        {
            try
            {
                con.Close();
                return true;

            }
            catch (Exception ex)
            {
                error = ex.ToString();

            }
            return false;
        }
    }
}