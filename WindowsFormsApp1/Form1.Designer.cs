﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.lblTitle = new System.Windows.Forms.Label();
            this.lblHrFecha = new System.Windows.Forms.Label();
            this.lblNombre = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.DGEstilo = new System.Windows.Forms.DataGridView();
            this.Precio = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Producto = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Cantidad = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Total = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEstilo = new System.Windows.Forms.TextBox();
            this.btnEstilo = new System.Windows.Forms.Button();
            this.btnEstilo1 = new System.Windows.Forms.Button();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.btnEstilo2 = new System.Windows.Forms.Button();
            this.btnEstilo4 = new System.Windows.Forms.Button();
            this.btnEstilo3 = new System.Windows.Forms.Button();
            this.pbEstiloIzq = new System.Windows.Forms.PictureBox();
            this.pbEstiloDer = new System.Windows.Forms.PictureBox();
            this.lblTotal = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.DGEstilo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEstiloIzq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEstiloDer)).BeginInit();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.AutoSize = true;
            this.lblTitle.Location = new System.Drawing.Point(179, 33);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(35, 13);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "label1";
            // 
            // lblHrFecha
            // 
            this.lblHrFecha.AutoSize = true;
            this.lblHrFecha.Location = new System.Drawing.Point(56, 363);
            this.lblHrFecha.Name = "lblHrFecha";
            this.lblHrFecha.Size = new System.Drawing.Size(35, 13);
            this.lblHrFecha.TabIndex = 1;
            this.lblHrFecha.Text = "label1";
            // 
            // lblNombre
            // 
            this.lblNombre.AutoSize = true;
            this.lblNombre.Location = new System.Drawing.Point(182, 239);
            this.lblNombre.Name = "lblNombre";
            this.lblNombre.Size = new System.Drawing.Size(35, 13);
            this.lblNombre.TabIndex = 2;
            this.lblNombre.Text = "label1";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // DGEstilo
            // 
            this.DGEstilo.AllowUserToAddRows = false;
            this.DGEstilo.AllowUserToDeleteRows = false;
            this.DGEstilo.BackgroundColor = System.Drawing.Color.SteelBlue;
            this.DGEstilo.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DGEstilo.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Precio,
            this.Producto,
            this.Cantidad,
            this.Total});
            this.DGEstilo.Location = new System.Drawing.Point(-11, 219);
            this.DGEstilo.Name = "DGEstilo";
            this.DGEstilo.ReadOnly = true;
            this.DGEstilo.RowHeadersVisible = false;
            this.DGEstilo.Size = new System.Drawing.Size(442, 221);
            this.DGEstilo.TabIndex = 3;
            // 
            // Precio
            // 
            this.Precio.HeaderText = "Precio";
            this.Precio.Name = "Precio";
            this.Precio.ReadOnly = true;
            // 
            // Producto
            // 
            this.Producto.HeaderText = "Producto";
            this.Producto.Name = "Producto";
            this.Producto.ReadOnly = true;
            // 
            // Cantidad
            // 
            this.Cantidad.HeaderText = "Cantidad";
            this.Cantidad.Name = "Cantidad";
            this.Cantidad.ReadOnly = true;
            // 
            // Total
            // 
            this.Total.HeaderText = "Total";
            this.Total.Name = "Total";
            this.Total.ReadOnly = true;
            // 
            // txtEstilo
            // 
            this.txtEstilo.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtEstilo.Location = new System.Drawing.Point(-14, 49);
            this.txtEstilo.Name = "txtEstilo";
            this.txtEstilo.Size = new System.Drawing.Size(303, 31);
            this.txtEstilo.TabIndex = 0;
            this.txtEstilo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtEstilo_KeyPress);
            // 
            // btnEstilo
            // 
            this.btnEstilo.Image = ((System.Drawing.Image)(resources.GetObject("btnEstilo.Image")));
            this.btnEstilo.Location = new System.Drawing.Point(351, 172);
            this.btnEstilo.Name = "btnEstilo";
            this.btnEstilo.Size = new System.Drawing.Size(198, 151);
            this.btnEstilo.TabIndex = 5;
            this.btnEstilo.UseVisualStyleBackColor = true;
            // 
            // btnEstilo1
            // 
            this.btnEstilo1.Image = ((System.Drawing.Image)(resources.GetObject("btnEstilo1.Image")));
            this.btnEstilo1.Location = new System.Drawing.Point(302, 1);
            this.btnEstilo1.Name = "btnEstilo1";
            this.btnEstilo1.Size = new System.Drawing.Size(198, 151);
            this.btnEstilo1.TabIndex = 6;
            this.btnEstilo1.UseVisualStyleBackColor = true;
            // 
            // btnEstilo2
            // 
            this.btnEstilo2.Image = ((System.Drawing.Image)(resources.GetObject("btnEstilo2.Image")));
            this.btnEstilo2.Location = new System.Drawing.Point(97, 62);
            this.btnEstilo2.Name = "btnEstilo2";
            this.btnEstilo2.Size = new System.Drawing.Size(198, 151);
            this.btnEstilo2.TabIndex = 7;
            this.btnEstilo2.UseVisualStyleBackColor = true;
            // 
            // btnEstilo4
            // 
            this.btnEstilo4.Image = ((System.Drawing.Image)(resources.GetObject("btnEstilo4.Image")));
            this.btnEstilo4.Location = new System.Drawing.Point(506, 1);
            this.btnEstilo4.Name = "btnEstilo4";
            this.btnEstilo4.Size = new System.Drawing.Size(198, 151);
            this.btnEstilo4.TabIndex = 8;
            this.btnEstilo4.UseVisualStyleBackColor = true;
            // 
            // btnEstilo3
            // 
            this.btnEstilo3.Image = ((System.Drawing.Image)(resources.GetObject("btnEstilo3.Image")));
            this.btnEstilo3.Location = new System.Drawing.Point(555, 219);
            this.btnEstilo3.Name = "btnEstilo3";
            this.btnEstilo3.Size = new System.Drawing.Size(198, 151);
            this.btnEstilo3.TabIndex = 9;
            this.btnEstilo3.UseVisualStyleBackColor = true;
            // 
            // pbEstiloIzq
            // 
            this.pbEstiloIzq.Image = ((System.Drawing.Image)(resources.GetObject("pbEstiloIzq.Image")));
            this.pbEstiloIzq.Location = new System.Drawing.Point(412, 293);
            this.pbEstiloIzq.Name = "pbEstiloIzq";
            this.pbEstiloIzq.Size = new System.Drawing.Size(116, 30);
            this.pbEstiloIzq.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbEstiloIzq.TabIndex = 10;
            this.pbEstiloIzq.TabStop = false;
            // 
            // pbEstiloDer
            // 
            this.pbEstiloDer.Image = ((System.Drawing.Image)(resources.GetObject("pbEstiloDer.Image")));
            this.pbEstiloDer.Location = new System.Drawing.Point(351, 487);
            this.pbEstiloDer.Name = "pbEstiloDer";
            this.pbEstiloDer.Size = new System.Drawing.Size(142, 30);
            this.pbEstiloDer.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pbEstiloDer.TabIndex = 11;
            this.pbEstiloDer.TabStop = false;
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(44, 148);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(76, 25);
            this.lblTotal.TabIndex = 12;
            this.lblTotal.Text = "label1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(777, 628);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.pbEstiloDer);
            this.Controls.Add(this.pbEstiloIzq);
            this.Controls.Add(this.btnEstilo3);
            this.Controls.Add(this.btnEstilo4);
            this.Controls.Add(this.btnEstilo2);
            this.Controls.Add(this.btnEstilo1);
            this.Controls.Add(this.btnEstilo);
            this.Controls.Add(this.txtEstilo);
            this.Controls.Add(this.DGEstilo);
            this.Controls.Add(this.lblNombre);
            this.Controls.Add(this.lblHrFecha);
            this.Controls.Add(this.lblTitle);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
            this.Text = "Form1";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Resize += new System.EventHandler(this.Form1_Resize);
            ((System.ComponentModel.ISupportInitialize)(this.DGEstilo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEstiloIzq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pbEstiloDer)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblHrFecha;
        private System.Windows.Forms.Label lblNombre;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.DataGridView DGEstilo;
        private System.Windows.Forms.TextBox txtEstilo;
        private System.Windows.Forms.Button btnEstilo;
        private System.Windows.Forms.Button btnEstilo1;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Button btnEstilo2;
        private System.Windows.Forms.Button btnEstilo4;
        private System.Windows.Forms.Button btnEstilo3;
        private System.Windows.Forms.PictureBox pbEstiloIzq;
        private System.Windows.Forms.PictureBox pbEstiloDer;
        private System.Windows.Forms.DataGridViewTextBoxColumn Precio;
        private System.Windows.Forms.DataGridViewTextBoxColumn Producto;
        private System.Windows.Forms.DataGridViewTextBoxColumn Cantidad;
        private System.Windows.Forms.DataGridViewTextBoxColumn Total;
        private System.Windows.Forms.Label lblTotal;
    }
}

